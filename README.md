Systemkern Simple Static Site Setup (S5)
========================================

S5 (Systemkern Simple Static Site Setup) facilitates the creation of websites using
only static HTML, taking away some of the pains that come with static HTML pages.

*S5* looks at your index.html finds the `<head>`, `<header>` and `<footer>` tags
and copies the content of those tags. It then replaces the content of all other 
HTML page's `<head>`, `<header>` and `<footer>` tags with the content from index.html   



Usage
--------------------
Run the `s5` command from your website root.
S5 will then automatically look for your index.html and start
replacing heads, headers and footers of the other html files with your source file

To run locally you have to have gsed installed: `brew install gnu-sed`

Alternative Usage
--------------------
To run S5 from a docker container use this command:
```
docker run --name=systemkern-s5-shell-alias-container --rm --tty --volume ${HOME}:/root --volume ${PWD}:/app registry.gitlab.com/systemkern/s5:latest s5
```


Alternative Usage 2
--------------------
Add the following alias to your `~/.bash_rc` or `~/.bash_profile` (depending on your OS) 

```
#   --rm                       # Automatically remove the container when it exits
#   --tty                      # Allocate a pseudo-TTY
#   --volume ${HOME}:/root	    # Bind mount host os user home directory to container root
#   --volume ${pwd}:/app       # Mount current directory to /app
#   s5:latest                  # Select docker image
alias run-in-s5-docker="docker run --name=systemkern-s5-shell-alias-container \
    --rm                        \
    --tty                       \
    --volume ${HOME}:/root      \
    --volume ${PWD}:/app        \
    registry.gitlab.com/systemkern/s5:latest"
```

then execute `run-in-s5-docker s5` or `bin/build-locally.sh && run-in-s5-docker s5` 


Build Locally 
--------------------
execute `bin/build-locally.sh`


Test Locally 
--------------------
execute `bin/build-locally.sh && run-in-s5-docker s5` 



Advanced Capabilities
--------------------

### Markdown Pre-rendering
Thanks to Chad Braun-Duin's [Markdown Bash](https://github.com/chadbraunduin/markdown.bash) all `*.md` Markdown files will be rendered to a simple HTML structure.
```html
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Title</title>
</head>
<body>
<header></header>
<main>
__ HTML RENDERED CONTENT OF THE MARKDOWN FILE __
</main>
<footer></footer>
</body>
</html>
``` 

This pre-rendering happens before all other `s5` code is executed. This way it is ensured that markdown files will be
handled exactly the  same as all other HTML files.
